# -*- coding: utf-8 -*-
from logging import getLogger
from re import compile

log = getLogger("MainWindow")
value_is_int = compile("[0-9+]")


def strToValue(str, kind):
    if kind == "bool" or kind == bool:
        if str is True:
            return True
        elif str is False:
            return False
        elif str.lower() == "true":
            return True
        else:
            return False

    if kind == "int" or kind == int:
        try:
            if value_is_int.match(str):
                return int(str)
            else:
                return 0
        except Exception as e:
            log.warning(
                "There was an error converting the value {} to int: {}".format(
                    str,
                    e
                )
            )
            return 0

    return str
