# -*- coding: utf-8 -*-

'''
18 Aug 2019
@autor: Daniel Carrasco
'''

from PIL import Image
from logging import getLogger

# ## Log Configuration ## #
log = getLogger("MainWindow")


def remove_transparency(im, bg_colour=(255, 255, 255)):
    try:
        # Only process if image has transparency
        # (http://stackoverflow.com/a/1963146)
        if (
            im.mode in ('RGBA', 'LA')
            or (im.mode == 'P' and 'transparency' in im.info)
        ):
            # Need to convert to RGBA if LA format due to a
            # bug in PIL (http://stackoverflow.com/a/1963146)
            alpha = im.convert('RGBA').split()[-1]
            # Create a new background image of our matt color.
            # Must be RGBA because paste requires both images have the
            # same format http://stackoverflow.com/a/8720632
            # and http://stackoverflow.com/a/9459208)
            bg = Image.new("RGBA", im.size, bg_colour + (255,))
            bg.paste(im, mask=alpha)
            return bg

        else:
            return im
    except Exception as e:
        log.error(
            "There was an error removing the transparency from the image:"
            " {}".format(e)
        )
        return False
