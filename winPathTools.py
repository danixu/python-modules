# -*- coding: utf-8 -*-

'''
18 Aug 2019
@autor: Daniel Carrasco
'''

import os
from collections import OrderedDict
from win32com.shell import shell
from win32api import GetFileAttributes
from logging import getLogger
from datetime import date
from subprocess import Popen, CREATE_NO_WINDOW

# ## Log Configuration ## #
log = getLogger("MainWindow")

# # CSIDL Values
CSIDL_Values = OrderedDict([
    ('MYDOCUMENTS', 5),
    ('LOCAL_APPDATA', 28),
    ('APPDATA', 26),
    ('DESKTOPDIRECTORY', 16),
    ('MYMUSIC', 13),
    ('MYPICTURES', 39),
    ('MYVIDEO', 14),
    ('PROFILE', 40),
    ('COMMON_APPDATA', 35),
    ('COMMON_DESKTOPDIRECTORY', 25),
    ('COMMON_DOCUMENTS', 46),
    ('COMMON_MUSIC', 53),
    ('COMMON_PICTURES', 54),
    ('COMMON_VIDEO', 55),
    ('PROGRAM_FILES_COMMON', 43),
    ('PROGRAM_FILES_COMMONX86', 44),
    ('PROGRAM_FILES', 38),
    ('PROGRAM_FILESX86', 42),
])


def folderToWindowsVariable(folder):
    for variable, value in CSIDL_Values.items():
        folder = folder.replace(
            shell.SHGetFolderPath(0, value, None, 0),
            "%{}%".format(variable)
        )
    return folder


def windowsVariableToFolder(folder):
    for variable, value in CSIDL_Values.items():
        folder = folder.replace(
            "%{}%".format(variable),
            shell.SHGetFolderPath(0, value, None, 0)
        )

    return folder


def makeSymbolicLink(src, dst):
    log.info("Creating symlink: {} -> {}".format(dst, src))
    try:
        if os.path.isdir(src):
            log.debug("Destination folder exists. Checking if is symlink.")
            # Check if is a symlink

            fileAttr = GetFileAttributes(src)

            for i in [4194304, 262144, 131072, 65536, 32768, 16384, 8192, 4096, 2048]:
                if fileAttr > i:
                    fileAttr -= i

            if fileAttr >= 1024:
                log.debug("Destination folder is symlink, deleting...")
                # If is a symlink, just remove it
                os.rmdir(src)
            else:
                log.debug("Destination folder is not a symlink, renaming...")
                # If not, rename the folder
                now = date.today().strftime("%Y%m%d_%H%M%S")
                newName = "{}-{}".format(src, now)
                log.debug("Renaming {} to {}".format(src, now))
                os.rename(src, newName)
        else:
            log.debug("Destination folder doesn't exists.")
            parent_folder = os.path.dirname(src.rstrip("/").rstrip("\\"))
            log.debug("Parent folder: {}".format(parent_folder))
            if not os.path.isdir(parent_folder):
                log.debug("Parent folder doesn't exists, so it will be created")
                os.makedirs(parent_folder)

        # Fails on windows, so I've used subprocess
        # os.symlink(src=folder, dst=dst, target_is_directory=True)
        # Check if is a symlink. Needs shell=True or will fail
        child = Popen(
            'mklink /J "{}" "{}"'.format(src, dst),
            creationflags=CREATE_NO_WINDOW,
            shell=True
        )
        strdout, stderr = child.communicate()
        if child.returncode != 0:
            return False

        return True

    except Exception as e:
        log.error("Error creating symlink: {}".format(e))
        return False
