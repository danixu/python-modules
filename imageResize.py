# -*- coding: utf-8 -*-
from io import BytesIO
from PIL import Image
# This import is to fix Frozen apps
# from PIL import GifImagePlugin, JpegImagePlugin, PngImagePlugin
from logging import getLogger
from os.path import isfile
from .imageRemoveTransparency import remove_transparency

log = getLogger("MainWindow")


# Image Formats
BITMAP_TYPE_PNG = 15
BITMAP_TYPE_JPEG = 17
BITMAP_TYPE_BMP = 1
BITMAP_TYPE_TIFF = 11


def imageResize(fName, nWidth=44, nHeight=44, out_format=BITMAP_TYPE_PNG,
                compression=None, centered=True, color=(255, 255, 255, 255)):
    if fName is not None and isfile(fName):
        # The file is saved to BytesIO and reopened because
        # if not, some ico files are not resized correctly
        try:
            log.debug("Creating BytesIO data")
            tmp_data = BytesIO()
            log.debug("Opening source image file")
            tmp_image = Image.open(fName)
            log.debug("Creating temporal data in memory to allow resize")
            tmp_image.save(tmp_data, "PNG", compress_level=1)
            log.debug("Closing the source image")
            tmp_image.close()
        except Exception as e:
            log.error("There was an error opening the image: {}".format(e))
            return False

        try:
            tmp_image = Image.open(tmp_data)
        except Exception as e:
            log.error("There was an error opening the image: {}".format(e))
            return False

        try:
            if tmp_image.size[0] < nWidth and tmp_image.size[1] < nHeight:
                log.debug("Size is smaller than indicated size")
                width, height = tmp_image.size

                if width > height:
                    factor = nWidth / width
                    width = nWidth
                    height = int(height * factor)

                    # if height%2 > 0:
                    #     height += 1

                    log.debug("Resizing image...")
                    tmp_image = tmp_image.resize((width, height), Image.LANCZOS)
                else:
                    factor = nHeight / height
                    width = int(width * factor)
                    height = nHeight

                    # if width%2 > 0:
                    #     height += 1

                    log.debug("Resizing image...")
                    tmp_image = tmp_image.resize((width, height), Image.LANCZOS)

            else:
                log.debug("The image is bigger than indicated size...")
                log.info("Creating thumbnail.")
                tmp_image.thumbnail((nWidth, nHeight), Image.LANCZOS)
        except Exception as e:
            log.error("There was an error resizing the image: {}".format(e))
            return False

        try:
            if centered and tmp_image.size[0] != tmp_image.size[1]:
                log.debug("Centering image...")
                new_image = Image.new(
                    "RGB" if out_format == BITMAP_TYPE_JPEG else "RGBA",
                    (nWidth, nHeight),
                    color
                )
                new_image.paste(
                    tmp_image,
                    (
                        int((nWidth-tmp_image.size[0])/2),
                        int((nHeight-tmp_image.size[1])/2)
                    )
                )
                tmp_image.close()
                tmp_image = new_image

        except Exception as e:
            log.error("There was an error centering the image: {}".format(e))
            return False

        try:
            if out_format == Image:
                return tmp_image
            else:
                tmp_data = BytesIO()
                if out_format == BITMAP_TYPE_PNG:
                    tmp_image.save(
                        tmp_data,
                        'png',
                        optimize=True,
                        compress_level=compression or 9
                    )

                elif out_format == BITMAP_TYPE_JPEG:
                    tmp_image = remove_transparency(tmp_image).convert('RGB')
                    tmp_image.save(
                        tmp_data,
                        'jpeg',
                        progressive=True,
                        optimize=True,
                        quality=compression or 85
                    )

                elif out_format == BITMAP_TYPE_BMP:
                    tmp_image = remove_transparency(tmp_image).convert('RGB')
                    tmp_image.save(
                        tmp_data,
                        'bmp',
                        compression=compression or 'bmp_rle'
                    )

                elif out_format == BITMAP_TYPE_TIFF:
                    tmp_image.save(
                        tmp_data,
                        'tiff',
                        compression='tiff_deflate',
                        quality=compression or 85
                    )

                del(tmp_image)
                return tmp_data

        except Exception as e:
            log.error("There was an error saving the image: {}".format(e))
            return False

    else:
        return None
