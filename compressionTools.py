# -*- coding: utf-8 -*-

'''
21 Aug 2019
@autor: Daniel Carrasco
'''

import bz2
import gzip
from logging import getLogger
from lz4 import frame as lz4
import lzma
from enum import IntEnum

log = getLogger("MainWindow")


class COMPRESSION_FMT(IntEnum):
    NONE = 0
    LZ4 = 1
    GZIP = 2
    BZ2 = 3
    LZMA = 4


default_format = COMPRESSION_FMT.NONE


def compressData(binData, format=default_format):
    if format == COMPRESSION_FMT.NONE:
        return binData
    elif format == COMPRESSION_FMT.LZ4:
        return lz4.compress(
            binData,
            compression_level=lz4.COMPRESSIONLEVEL_MAX,
            content_checksum=True
        )
    elif format == COMPRESSION_FMT.GZIP:
        return gzip.compress(
            binData,
            compresslevel=9
        )
    elif format == COMPRESSION_FMT.BZ2:
        return bz2.compress(
            binData,
            compresslevel=9
        )
    elif format == COMPRESSION_FMT.LZMA:
        lzma_format = lzma.FORMAT_XZ
        lzma_filters = [
            {"id": lzma.FILTER_X86},
            {"id": lzma.FILTER_DELTA, "dist": 1},
            {"id": lzma.FILTER_LZMA2, "preset": 9 | lzma.PRESET_EXTREME},
        ]
        return lzma.compress(
            binData,
            format=lzma_format,
            check=lzma.CHECK_SHA256,
            filters=lzma_filters
        )


def decompressData(binData, format=default_format):
    log.debug(
        "Decompressing data with format {}".format(
            COMPRESSION_FMT(format).name
        )
    )
    if format == COMPRESSION_FMT.NONE:
        return binData
    elif format == COMPRESSION_FMT.LZ4:
        return lz4.decompress(binData)
    elif format == COMPRESSION_FMT.GZIP:
        return gzip.decompress(binData)
    elif format == COMPRESSION_FMT.BZ2:
        return bz2.decompress(binData)
    elif format == COMPRESSION_FMT.LZMA:
        return lzma.decompress(binData)
