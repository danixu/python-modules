# -*- coding: utf-8 -*-

'''
18 Aug 2019
@autor: Daniel Carrasco
'''

from os.path import dirname, realpath, join
import sys
from logging import getLogger

# ## Log Configuration ## #
log = getLogger("MainWindow")

global rootPath
if getattr(sys, 'frozen', False):
    # The application is frozen
    rootPath = dirname(realpath(sys.executable))
else:
    # The application is not frozen
    # Change this bit to match where you store your data files:
    rootPath = dirname(realpath(__file__))


def fullPath(fPath):
    if ":" in fPath or fPath[0] == "/":
        return fPath
    else:
        return join(rootPath, fPath)


def relativePath(fPath):
    if ":" in fPath or fPath[0] == "/":
        return fPath.replace("{}\\".format(rootPath.rstrip("\\")), "")
    else:
        return fPath
