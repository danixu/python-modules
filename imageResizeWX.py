# -*- coding: utf-8 -*-
from io import BytesIO
from logging import getLogger
from os.path import isfile
from wx import (
    Image,
    PNGHandler,
    IMAGE_QUALITY_HIGH,
    IMAGE_OPTION_PNG_COMPRESSION_LEVEL,
    IMAGE_OPTION_PNG_COMPRESSION_MEM_LEVEL,
    IMAGE_OPTION_QUALITY,
    IMAGE_OPTION_TIFF_COMPRESSION
)

log = getLogger("MainWindow")

# Adding Handlers
Image.AddHandler(PNGHandler())


# Image Formats
BITMAP_TYPE_PNG = 15
BITMAP_TYPE_JPEG = 17
BITMAP_TYPE_BMP = 1
BITMAP_TYPE_TIFF = 11


def imageResizeWX(fName, nWidth=44, nHeight=44, out_format=BITMAP_TYPE_PNG,
                  compression=None, centered=True, color=(255, 255, 255)):
    if fName is not None and isfile(fName):
        try:
            tmp_image = Image()
            tmp_image.LoadFile(fName)
        except Exception as e:
            log.error("There was an error opening the image: {}".format(e))
            return False

        try:
            log.debug("Size is smaller than indicated size")
            width, height = tmp_image.GetSize()

            if width > height:
                factor = nWidth / width
                width = nWidth
                height = int(height * factor)

                # if height%2 > 0:
                #     height += 1

                log.debug("Resizing image...")
                tmp_image = tmp_image.Rescale(
                    width,
                    height,
                    IMAGE_QUALITY_HIGH
                )
            else:
                factor = nHeight / height
                width = int(width * factor)
                height = nHeight

                # if width%2 > 0:
                #     height += 1

                log.debug("Resizing image...")
                tmp_image = tmp_image.Rescale(
                    width,
                    height,
                    IMAGE_QUALITY_HIGH
                )

        except Exception as e:
            log.error("There was an error resizing the image: {}".format(e))
            return False

        # If image is transparent and format doesn't support it,
        # remove transparency
        try:
            if (
                (out_format == BITMAP_TYPE_JPEG
                 or out_format == BITMAP_TYPE_BMP)
                and tmp_image.HasAlpha()
            ):
                log.debug("Removing alpha channel with color {}".format(color))
                tmp_image.ConvertAlphaToMask(
                    color[0],
                    color[1],
                    color[2],
                    90
                )

        except Exception as e:
            log.error("There was an error removing alpha channel: {}".format(e))

        try:
            if centered and tmp_image.GetSize()[0] != tmp_image.GetSize()[1]:
                log.debug("Centering image...")
                tmp_image = tmp_image.Resize(
                    (
                        nWidth,
                        nHeight
                    ),
                    (
                        int((nWidth-tmp_image.GetSize()[0])/2),
                        int((nHeight-tmp_image.GetSize()[1])/2)
                    ),
                    color[0],
                    color[1],
                    color[2]
                )

        except Exception as e:
            log.error("There was an error centering the image: {}".format(e))
            return False

        try:
            if out_format == Image:
                return tmp_image
            else:
                tmp_data = BytesIO()
                if out_format == BITMAP_TYPE_PNG:
                    tmp_image.SetOption(
                        IMAGE_OPTION_PNG_COMPRESSION_LEVEL,
                        compression or 9
                    )
                    tmp_image.SetOption(
                        IMAGE_OPTION_PNG_COMPRESSION_MEM_LEVEL, 9
                    )

                elif out_format == BITMAP_TYPE_JPEG:
                    tmp_image.SetOption(
                        IMAGE_OPTION_QUALITY,
                        compression or 85
                    )

                elif out_format == BITMAP_TYPE_TIFF:
                    tmp_image.SetOption(
                        IMAGE_OPTION_TIFF_COMPRESSION,
                        compression or 5
                    )

                tmp_image.SaveFile(tmp_data, out_format)
                del(tmp_image)
                return tmp_data

        except Exception as e:
            log.error("There was an error saving the image: {}".format(e))
            return False

    else:
        return None
