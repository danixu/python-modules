# -*- coding: utf-8 -*-

from logging import getLogger
from configparser import ConfigParser

log = getLogger("MainWindow")


def LoadConfigToDict(fName, config={}):
    """
    returns a dictionary with keys of the form
    <section>:{
        <option>: values
    }
    """
    try:
        config = config.copy()
        cp = ConfigParser()
        cp.read(fName, encoding='utf-8')
        for sec in cp.sections():
            name = sec.lower()
            if not config.get(name, False):
                config[name] = {}
            for opt in cp.options(sec):
                try:
                    config[name][opt.lower()] = int(cp.get(sec, opt).strip())

                except Exception as e:
                    log.debug("Value is not int: {}".format(e))
                    config[name][opt.lower()] = cp.get(sec, opt).strip()
        return config

    except Exception as e:
        log.error("There was an error loading config file: {}".format(e))
        return False


def SaveConfigFromDict(fName, data):
    """
    Converts a dictionary with keys to INI format
    """
    try:
        cp = ConfigParser()
        for section, section_data in data.items():
            cp.add_section(section)
            for key, value in section_data.items():
                cp.set(section, key, str(value))

        with open('config.ini', 'w', encoding='utf-8') as configfile:
            cp.write(configfile)
        return True

    except Exception as e:
        log.error("There was an error loading config file: {}".format(e))
        return False
