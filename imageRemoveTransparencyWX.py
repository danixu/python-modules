# -*- coding: utf-8 -*-

'''
18 Aug 2019
@autor: Daniel Carrasco
'''

from logging import getLogger

# ## Log Configuration ## #
log = getLogger("MainWindow")


def remove_transparency(im, bg_colour=(255, 255, 255)):
    try:
        if im.HasAlpha():
            im.ConvertAlphaToMask()
            return im

        else:
            return im
    except Exception as e:
        log.error(
            "There was an error removing the transparency from the image:" +
            " {}".format(e)
        )
        return False
