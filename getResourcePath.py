# -*- coding: utf-8 -*-
from os.path import join, dirname, realpath
import sys


def getResourcePath(fPath, fName, internal=True):
    """
        This function get the internal path for resources.
        This means the temporal path if exe is packed, or the root path
        if python path is executed or exe is not packed.
        Pass the internal argument as False allow to get always the root path
    """
    if getattr(sys, '_MEIPASS', False) and internal:
        return join(sys._MEIPASS, fPath, fName)
    else:
        global rootPath
        if getattr(sys, 'frozen', False):
            # The application is frozen
            rootPath = dirname(realpath(sys.executable))
        else:
            # The application is not frozen
            # Change this bit to match where you store your data files:
            rootPath = dirname(realpath(sys.modules['__main__'].__file__))

        return join(rootPath, fPath, fName)
