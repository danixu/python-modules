from math import ceil


def hash_to_iterations(hash, digits):
    actual = 0
    split = ceil(len(hash) / digits)
    digits_list = []
    while actual < len(hash):
        start = actual
        end = actual + split
        current_digit = 0
        for letter in hash[start:end]:
            current_digit += ord(letter)

        current_digit = str(current_digit)
        while len(current_digit) > 1:
            new_digit = 0
            for num in current_digit:
                new_digit += int(num)

            current_digit = str(new_digit)

        digits_list.append(str(current_digit))
        actual = end

    return int("".join(digits_list))
